<#compress>
<div class="comment">
    <div class="comment_user">
        <a href="#" class="comment_avatar">
            <#include "UserAvatar.ftl">
        </a>
        <#include "UserInfo.ftl">
    </div>
    <div class="comment_body">
        <div class="comment_content">
            <div class="comment_text">${commentText}</div>
        </div>
    </div>
</div>
</#compress>
