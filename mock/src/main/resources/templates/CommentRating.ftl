<#compress>
    <div class="comment_rating-control __increase js-increase">+</div>

    <#if (currentValue > 100)>
        <div class="comment_rating-value __positive">
    <#else>
        <div class="comment_rating-value">
    </#if>
        ${currentValue}
    </div>
    <div class="comment_rating-control __decrease js-decrease">&minus;</div>
</#compress>