<#compress>
<div class="header_search-section">
    <#if (!response?has_content)>
        Результатов не нашлось
    </#if>

    <#if (response?has_content)>
        <div class="header_search-section-title">Новости</div>
        <div class="header_search-section-cnt">
            <#list response as news>
                <div class="header_search-section-item">
                    <#assign link = "<a href='${news.link}' class='header_search-section-item-a'>${news.title}</a>">
                    ${link}
                </div>
            </#list>
            <div class="header_search-section-item">
                <a href="#" class="header_search-section-item-a __sec">Все новости...</a>
            </div>
        </div>
    </#if>
</div>
</#compress>