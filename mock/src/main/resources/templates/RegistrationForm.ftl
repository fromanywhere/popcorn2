<#compress>
<div class="registration">
    <div class="registration_form">
        <form class="form">
            <div class="form_i">
                <label class="form_label" for="email">E-mail</label>
                <input class="base-input" type="email" id="email" name="email" placeholder="E-mail" />
            </div>
            <div class="form_i">
                <label class="form_label" for="password">Пароль</label>
                <input class="base-input" type="password" id="password" name="password" placeholder="Пароль" />
            </div>

            <div class="registration_footer">
                <#if (formError)>
                    <div class="registration_error">
                        <div class="form_error">
                            Неверное имя или пароль
                        </div>
                    </div>
                </#if>

                <div class="registration_actions">
                    <input type="hidden" name="submit" value="true" />
                    <button class="button __wide" type="submit">Зарегистрироваться</button>
                </div>
            </div>
        </form>
    </div>
    <div class="login_footer">
        <div class="login_footer-cnt">
            Есть логин? <a class="login_footer-cnt-link js-login" href="#">Войдите</a>
        </div>

        <div class="login_footer-note">
            Авторизовываясь, вы соглашаетесь с <a class="login_footer-link" href="#">правилами использования сайта</a>
        </div>
    </div>
</div>
</#compress>