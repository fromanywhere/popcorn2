<#compress>
    <#if (favorite)>
        <div class="responsive-switcher">
            <div class="responsive-switcher_default">
                <span class="icon __inline icon_heart"></span> в избранном
            </div>
            <div class="responsive-switcher_tablet">
                <a class="button">
                    <svg class="icon __inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 14" width="15" height="14">
                        <g fill-rule="evenodd" stroke-width="0">
                            <path fill-rule="evenodd" d="M36.52 43.05a3.843 3.843 0 0 0-5.401 0 3.766 3.766 0 0 0 0 5.357l6.382 6.328.638-.633 4.764-4.723.978-.972a3.766 3.766 0 0 0 0-5.357 3.843 3.843 0 0 0-5.401 0l-.98.972-.98-.972z" transform="translate(-30 -41)"></path>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </#if>

    <#if (!favorite)>
        <a class="button">
            <svg class="icon __inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 14" width="15" height="14">
                <g fill-rule="evenodd" stroke-width="0">
                    <path fill-rule="evenodd" d="M36.52 43.05a3.843 3.843 0 0 0-5.401 0 3.766 3.766 0 0 0 0 5.357l6.382 6.328.638-.633 4.764-4.723.978-.972a3.766 3.766 0 0 0 0-5.357 3.843 3.843 0 0 0-5.401 0l-.98.972-.98-.972z" transform="translate(-30 -41)"></path>
                </g>
            </svg>
            &nbsp;
            В избранное
        </a>
    </#if>
</#compress>