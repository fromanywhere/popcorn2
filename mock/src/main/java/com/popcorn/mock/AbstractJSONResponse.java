package com.popcorn.mock;

public class AbstractJSONResponse {
    private boolean error;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
