package com.popcorn.mock.SearchForm;

import java.io.Serializable;
import java.util.List;

public class GoogleSearchResponse implements Serializable {
    private List<GoogleSearchResponseBean> items;

    public List<GoogleSearchResponseBean> getItems() {
        return items;
    }

    public void setItems(List<GoogleSearchResponseBean> items) {
        this.items = items;
    }
}
