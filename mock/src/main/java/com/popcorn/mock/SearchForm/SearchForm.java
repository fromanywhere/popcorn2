package com.popcorn.mock.SearchForm;

import java.util.List;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class SearchForm extends AbstractJSONRenderResponse {
    private List<GoogleSearchResponseBean> response;

    public SearchForm(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    public List<GoogleSearchResponseBean> getResponse() {
        return response;
    }

    public SearchForm setResponse(List<GoogleSearchResponseBean> response) {
        this.response = response;
        return this;
    }
}
