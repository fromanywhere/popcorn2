package com.popcorn.mock.SearchForm;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class SearchFormEndpoint {

    @Autowired
    private SearchForm searchForm;

    @CrossOrigin
    @RequestMapping("/search/fasthtml")
    public SearchForm commentPosting(
            @RequestParam(value="query", defaultValue="") String query
    ) {
        GoogleSearchResponse response = null;

        if (!StringUtils.isEmpty(query)) {
            RestTemplate restTemplate = new RestTemplate();
            String queryHost = "https://www.googleapis.com/customsearch/v1?key=AIzaSyBc263F7dUnNydKFt-Qbgj-XZ8ljEXARzA&cx=014787643169654740371:um59zrshaso&q=";
            response = restTemplate.getForObject(queryHost + query, GoogleSearchResponse.class);
        }

        return searchForm
            .setResponse(response != null ? response.getItems() : Collections.emptyList());
    }
}
