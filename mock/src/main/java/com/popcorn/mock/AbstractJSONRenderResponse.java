package com.popcorn.mock;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbstractJSONRenderResponse extends AbstractJSONResponse implements RenderableJSON {
    private final TemplateBuilder templateBuilder;

    @Autowired
    public AbstractJSONRenderResponse(TemplateBuilder templateBuilder) {
        this.templateBuilder = templateBuilder.setPath(getTemplateName());
    }

    protected String getTemplateName() {
        return getClass().getSimpleName() + ".ftl";
    }

    public String getResultHTML() {
        Map<String, Object> model = new HashMap<>();
        try {
            Field[] declaredFields = getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                field.setAccessible(true);
                model.put(field.getName(), field.get(this));
            }
        } catch (IllegalAccessException e) {
            this.setError(true);
            e.printStackTrace();
        }

        return this.templateBuilder.setModel(model).build();
    }
}
