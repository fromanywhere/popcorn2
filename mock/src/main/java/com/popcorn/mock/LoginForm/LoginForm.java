package com.popcorn.mock.LoginForm;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class LoginForm extends AbstractJSONRenderResponse {
    private boolean formError;
    private boolean success;

    public LoginForm(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    public boolean isSuccess() {
        return success;
    }

    public LoginForm setFormError(boolean formError) {
        this.formError = formError;
        return this;
    }

    public LoginForm setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
