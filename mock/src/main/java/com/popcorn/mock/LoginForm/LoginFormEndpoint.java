package com.popcorn.mock.LoginForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginFormEndpoint {
    private final LoginForm loginForm;

    @Autowired
    public LoginFormEndpoint(LoginForm loginForm) {
        this.loginForm = loginForm;
    }

    @CrossOrigin
    @RequestMapping("/loginForm")
    public LoginForm commentPosting(
            @RequestParam(value="email", defaultValue="") String email,
            @RequestParam(value="password", defaultValue="") String password,
            @RequestParam(value="submit", defaultValue="false") boolean submit
    ) {
        return loginForm
            .setFormError(submit && (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)))
            .setSuccess(submit && !StringUtils.isEmpty(email) && !StringUtils.isEmpty(password));
    }
}
