package com.popcorn.mock.FavoritePerson;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class FavoritePerson extends AbstractJSONRenderResponse {
    private boolean favorite;
    private boolean anonym;

    public FavoritePerson(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    public boolean isFavorite() {
        return favorite;
    }

    public FavoritePerson setFavorite(boolean favorite) {
        this.favorite = favorite;
        return this;
    }

    public boolean isAnonym() {
        return anonym;
    }

    public FavoritePerson setAnonym(boolean anonym) {
        this.anonym = anonym;
        return this;
    }
}
