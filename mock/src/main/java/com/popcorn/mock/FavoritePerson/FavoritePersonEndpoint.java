package com.popcorn.mock.FavoritePerson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.popcorn.mock.AbstractJSONResponse;

@RestController
public class FavoritePersonEndpoint {

    @Autowired
    private FavoritePerson favoritePerson;

    @CrossOrigin
    @RequestMapping("/favoritePerson")
    public AbstractJSONResponse favoritePerson(
            @RequestParam(value="isFavorite", defaultValue="false") boolean isFavorite
    ) {
        return favoritePerson
            .setAnonym(Math.random() < 0.5)
            .setFavorite(isFavorite);
    }
}
