package com.popcorn.mock;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

@Component
public class LocalFreeMarkerConfiguration extends Configuration {
    public LocalFreeMarkerConfiguration() {
        super(Configuration.VERSION_2_3_27);

        try {
            this.setDirectoryForTemplateLoading(new File("./src/main/resources/templates"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setDefaultEncoding("UTF-8");
        this.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        this.setLogTemplateExceptions(false);
        this.setWrapUncheckedExceptions(true);
        this.setWhitespaceStripping(true);
    }
}
