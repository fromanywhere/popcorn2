package com.popcorn.mock;

public interface RenderableJSON {
    String getResultHTML();
}
