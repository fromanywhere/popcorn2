package com.popcorn.mock;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import freemarker.template.Template;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TemplateBuilder {
    private String path;
    private Map<String, Object> model;

    private final LocalFreeMarkerConfiguration localFreeMarkerConfiguration;

    @Autowired
    public TemplateBuilder(LocalFreeMarkerConfiguration localFreeMarkerConfiguration) {
        this.localFreeMarkerConfiguration = localFreeMarkerConfiguration;
    }

    public TemplateBuilder setPath(String path) {
        this.path = path;
        return this;
    }

    public TemplateBuilder setModel(Map<String, Object> model) {
        this.model = model;
        return this;
    }

    public String build() {
        Writer out = new StringWriter();

        try {
            Template commentTemplate = localFreeMarkerConfiguration.getTemplate(path);
            commentTemplate.process(model, out);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return out
            .toString()
            .replace("\n", "");
    }
}
