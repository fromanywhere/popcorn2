package com.popcorn.mock.CommentRating;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class CommentRating extends AbstractJSONRenderResponse {
    private Long commentId;
    private Integer currentValue = 100;

    public CommentRating(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    public CommentRating init(Long commentId) {
        this.commentId = commentId;
        return this;
    }

    public CommentRating change(Short change) {
        if (change != 0) {
            this.setError(false);
            this.currentValue = this.currentValue + Math.round(Math.abs(change) / change);
        } else {
            this.setError(true);
        }
        return this;
    }
}
