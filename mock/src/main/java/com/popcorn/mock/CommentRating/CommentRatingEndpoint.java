package com.popcorn.mock.CommentRating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentRatingEndpoint {
    private final CommentRating commentRating;

    @Autowired
    public CommentRatingEndpoint(CommentRating commentRating) {
        this.commentRating = commentRating;
    }

    @CrossOrigin
    @RequestMapping("/commentRating")
    public CommentRating commentRating(
        @RequestParam(value="commentId", defaultValue="0") Long commentId,
        @RequestParam(value="change", defaultValue="0") Short change
    ) {
        return commentRating
            .init(commentId)
            .change(change);
    }
}
