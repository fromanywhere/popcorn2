package com.popcorn.mock.LinkShare;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class LinkShare extends AbstractJSONRenderResponse {
    public LinkShare(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }
}
