package com.popcorn.mock.LinkShare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LinkShareEndpoint {

    @Autowired
    private LinkShare linkShare;

    @CrossOrigin
    @RequestMapping("/linkShare")
    public LinkShare favoritePerson() {
        return linkShare;
    }
}

