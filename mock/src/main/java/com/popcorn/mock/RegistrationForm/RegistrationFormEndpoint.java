package com.popcorn.mock.RegistrationForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationFormEndpoint {
    private final RegistrationForm registrationForm;

    @Autowired
    public RegistrationFormEndpoint(RegistrationForm registrationForm) {
        this.registrationForm = registrationForm;
    }

    @CrossOrigin
    @RequestMapping("/registrationForm")
    public RegistrationForm commentPosting(
            @RequestParam(value="email", defaultValue="") String email,
            @RequestParam(value="password", defaultValue="") String password,
            @RequestParam(value="submit", defaultValue="false") boolean submit
    ) {
        return registrationForm
                .setFormError(submit && (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)))
                .setSuccess(submit && !StringUtils.isEmpty(email) && !StringUtils.isEmpty(password));
    }
}
