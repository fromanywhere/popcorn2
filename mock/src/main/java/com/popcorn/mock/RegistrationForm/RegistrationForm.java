package com.popcorn.mock.RegistrationForm;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class RegistrationForm extends AbstractJSONRenderResponse {
    private boolean formError;
    private boolean success;

    public RegistrationForm(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    public boolean isSuccess() {
        return success;
    }

    public RegistrationForm setFormError(boolean formError) {
        this.formError = formError;
        return this;
    }

    public RegistrationForm setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
