package com.popcorn.mock.CommentPosting;

import org.springframework.stereotype.Component;

import com.popcorn.mock.AbstractJSONRenderResponse;
import com.popcorn.mock.TemplateBuilder;

@Component
public class CommentPosting extends AbstractJSONRenderResponse {
    private Long discussionId;
    private String commentText;
    private String author = "Name";

    public CommentPosting(TemplateBuilder templateBuilder) {
        super(templateBuilder);
    }

    @Override
    protected String getTemplateName() {
        return "Comment.ftl";
    }

    public CommentPosting setCommentText(String commentText) {
        this.commentText = commentText;
        return this;
    }

    public CommentPosting setDiscussionId(Long discussionId) {
        this.discussionId = discussionId;
        return this;
    }
}
