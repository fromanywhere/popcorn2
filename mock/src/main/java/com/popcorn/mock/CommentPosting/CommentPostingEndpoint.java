package com.popcorn.mock.CommentPosting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentPostingEndpoint {
    private final CommentPosting commentPosting;

    @Autowired
    public CommentPostingEndpoint(CommentPosting commentPosting) {
        this.commentPosting = commentPosting;
    }

    @CrossOrigin
    @RequestMapping("/commentPosting")
    public CommentPosting commentPosting(
            @RequestParam(value="discussionId", defaultValue="0") Long discussionId,
            @RequestParam(value="commentText", defaultValue="") String commentText
    ) {
        return commentPosting
            .setDiscussionId(discussionId)
            .setCommentText(commentText);
    }
}
