/* global define */
define([
    './AbstractModule'
], function (
    AbstractModule
) {
    const TOP_SHIFT = 50;
    const DURATION = 300;

    /**
     * @class
     * @extends AbstractModule
     */
    class ScrollTo extends AbstractModule {

        animate(destinationOffset) {
            const that = this;
            const start = window.pageYOffset;
            const startTime = new Date().getTime();
            const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
            const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

            const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight
                ? documentHeight - windowHeight
                : destinationOffset
            );

            if ('requestAnimationFrame' in window === false) {
                window.scroll(0, destinationOffsetToScroll);
                that.scrollFinallyCallback();
                return;
            }

            function easeInOutCubic (t) {
                return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
            }

            function scroll() {
                const now = new Date().getTime();
                const time = Math.min(1, ((now - startTime) / DURATION));
                const timeFunction = easeInOutCubic(time);
                window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

                if (window.pageYOffset === destinationOffsetToScroll) {
                    that.scrollFinallyCallback();
                    return;
                }

                requestAnimationFrame(scroll);
            }

            scroll();
        }

        getTargetElementPosition() {
            let yPosition = 0;
            let element = this.targetElement;

            while(element) {
                yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
                element = element.offsetParent;
            }

            return yPosition;
        }

        clickHandler(e) {
            e.preventDefault();
            let top = 0;
            if (this.targetElement) {
                top = this.getTargetElementPosition() - TOP_SHIFT;
            }
            this.animate(top);
        }

        scrollFinallyCallback() {
            if (this.targetElement && this.targetElement.id) {
                window.history.replaceState({}, null, '#' + this.targetElement.id);
            }
        }

        getTargetElement(element) {
            const href = element.getAttribute('href');

            if (href) {
                const parts = href.split('#');
                if (parts.length > 1) {
                    const anchor = parts[1];
                    return document.getElementById(anchor);
                }
            }
        }

        activate(element) {
            this.targetElement = this.getTargetElement(element);
            if (this.targetElement) {
                this.boundClick = this.clickHandler.bind(this);
                element.addEventListener('click', this.boundClick);
            }
        }

    }

    return ScrollTo;
});