/* global define */

define([
    '../AbstractModule',
    '../AjaxRequest',
    '../layer/MessageLayerHelper',
], function (
    AbstractModule,
    AjaxRequest,
    MessageLayerHelper
) {

    class CommentForm extends AbstractModule {

        updateCommentsList(response) {
            const wrapper = document.createElement('div');
            wrapper.className = "comments-list_item";
            wrapper.innerHTML = response.resultHTML;

            this.commentsList.appendChild(wrapper);
        }

        handleEvent(event) {
            if (event.type === 'submit') {
                event.preventDefault();

                const message = event.target.elements['message'].value;
                
                this.ajaxRequest
                    .send({
                        discussionId: this.discussionId,
                        commentText: message
                    })
                    .then(this.updateCommentsList.bind(this))
                    .catch(MessageLayerHelper.showResponseError)
            }
        }

        activate(element) {
            this.element = element;
            this.discussionId = this.element.getAttribute('data-discussion-id');
            this.commentsList = this.element.querySelector('.comments-list');

            if (this.commentsList) {
                this.ajaxRequest = new AjaxRequest("/commentPosting");
                this.element.addEventListener('submit', this);
            }
        }
    }
    
    return CommentForm;
});