/* global define */

define([
    '../AbstractModule',
    '../AjaxRequest',
    '../dom/Delegate',
    '../layer/MessageLayerHelper'
], function (
    AbstractModule,
    AjaxRequest,
    Delegate,
    MessageLayerHelper
) {

    class CommentRating extends AbstractModule {
        constructor() {
            super();
            this.delegate = new Delegate();
        }

        update(change) {
            this.ajaxRequest.send({
                commentId: this.commentId,
                change
            }).then((response) => {
                this.element.innerHTML = response.resultHTML;
            }).catch(MessageLayerHelper.showResponseError)
        }

        activate(element) {
            this.element = element;
            this.commentId = element.getAttribute('data-comment-id');
            this.ajaxRequest = new AjaxRequest('/commentRating');

            this.delegate.root(element);
            this.delegate.on('click', '.js-increase', this.update.bind(this, 1));
            this.delegate.on('click', '.js-decrease', this.update.bind(this, -1));
        }
    }

    return CommentRating;
});