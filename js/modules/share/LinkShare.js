/*global define*/
define([
    '../AbstractModule',
    '../layer/LayerManager'
], function (
    AbstractModule,
    LayerManager
) {

    class LinkShare extends AbstractModule {

        activate(element) {
            this.link = element.getAttribute('data-link');
            this.title = element.getAttribute('data-title');            
            element.addEventListener('click', this);
        }

        handleEvent(e) {
            if (e.type === 'click') {
                LayerManager.open('ShareLinkLayer', {
                    link: this.link,
                    title: this.title
                });
            }
        }
    }

    return LinkShare;
});