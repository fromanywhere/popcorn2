/* global define */
define([
    './AbstractModule',
    './utils'
], function (
    AbstractModule,
    utils
) {
    /**
     * @class
     * @extends AbstractModule
     */
    class Toolbar extends AbstractModule {

        openSearch() {
            this.element.classList.add('__search-active');
            this.textInput.focus();
        }

        closeSearch() {
            this.textInput.value = '';
            this.searchResult.innerHTML = '';
            this.element.classList.remove('__search-active');
        }

        checkNavigation() {
            this.element.classList.toggle('__full-navigation', window.pageYOffset > 50)
        }

        checkForEscape(e) {
            if (e.code.toLowerCase() === 'escape') {
                this.closeSearch();
            }
        }

        activate(element) {
            this.element = element;
            this.searchIcon = element.querySelector('.header_search-controls-search');
            this.closeIcon = element.querySelector('.header_search-controls-cancel');
            this.textInput = element.querySelector('.header_search-text');
            this.searchResult = element.querySelector('.header_search-suggest');

            this.hasLargeNavigation = !!document.getElementById('header-navigation');

            this.searchIcon.addEventListener('click', this.openSearch.bind(this));
            this.closeIcon.addEventListener('click', this.closeSearch.bind(this));
            this.textInput.addEventListener('keydown', this.checkForEscape.bind(this));

            if (this.hasLargeNavigation) {
                window.addEventListener('scroll', utils.throttle(this.checkNavigation, 100, this, true));
                this.checkNavigation();
            }
        }
    }

    return Toolbar;
});