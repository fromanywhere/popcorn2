/*global define*/
define([
    '../AbstractModule',
    '../AjaxRequest',
    '../layer/LayerManager'
], function (
    AbstractModule,
    AjaxRequest,
    LayerManager
) {
    const FAVORITE_MOD = '__is-favorite';

    class PersonCardFavorite extends AbstractModule {

        activate(element) {
            this.element = element;
            this.isFavorite = element.classList.contains(FAVORITE_MOD);
            this.request = new AjaxRequest('/favoritePerson');
            element.addEventListener('click', this);
        }

        processResponse(response) {
            if (!response.error) {
                if (response.anonym) {
                    LayerManager.open('LoginLayer');
                } else {
                    this.isFavorite = response.favorite;
                    this.element.classList.toggle(FAVORITE_MOD, this.isFavorite);
                    this.element.innerHTML = response.resultHTML;
                }
            }
        }

        handleEvent(e) {
            if (e.type === 'click') {
                this.request.send({
                    isFavorite: !this.isFavorite
                }).then((response) => this.processResponse(response))
            }
        }
    }

    return PersonCardFavorite;
});