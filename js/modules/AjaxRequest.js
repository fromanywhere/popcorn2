/* global define, Promise */
define(function () {

    class AjaxRequest {

        constructor(url) {
            this.url = "http://127.0.0.1:8080" + url;

            this.success = () => {};
            this.error = () => {};
        }

        _processLoad() {
            if (this.xhr.status < 200 || this.xhr.status >= 400) {
                this.error({
                    error: true,
                    errorText: "Ошибка обработки запроса"
                });
                return;
            }

            try {
                const response = JSON.parse(this.xhr.responseText);
                if (response.error) {
                    this.error(response);
                } else {
                    this.success(response);                
                }
            } catch (e) {
                this.error({
                    error: true,
                    errorText: "Ошибка формата ответа"
                })
            }
        }

        _processError() {
            this.error({
                error: true,
                errorText: "Ошибка сетевого запроса"
            })    
        }

        send(data = {}) {
            if (this.xhr) {
                this.xhr.abort();
            }

            this.xhr = new XMLHttpRequest();
            let formData;

            if (data instanceof HTMLFormElement) {
                formData = new FormData(data);
            } else {
                formData = new FormData();
                for (let key in data) {
                    if (data.hasOwnProperty(key)) {
                        formData.append(key, data[key]);
                    }
                }
            }

            this.xhr.onload = this._processLoad.bind(this);
            this.xhr.onerror = this._processError.bind(this);

            this.xhr.open("POST", this.url);
            this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            this.xhr.send(formData);

            return new Promise((resolve, reject) => {
                this.success = resolve;
                this.error = reject;
            });
        }
    }

    return AjaxRequest;
});