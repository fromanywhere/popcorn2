/* global define */
define([
    './AbstractModule',
    './layer/LoginLayer'
], function (
    AbstractModule,
    LoginLayer
) {
    class LoginButton extends AbstractModule {

        activate(element) {
            element.addEventListener('click', () => {
                LoginLayer.open();
            })
        }
    }

    return LoginButton;
});