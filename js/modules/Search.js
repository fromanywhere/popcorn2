/* global define */
define([
    './AbstractModule',
    './AjaxRequest',
    './utils'
], function (
    AbstractModule,
    AjaxRequest,
    utils
) {

    class Search extends AbstractModule {

        activate(element) {
            this.request = new AjaxRequest('/search/fasthtml');
            this.form = element.querySelector('.header_search-input');
            this.searchInput = this.form.elements['query'];
            this.suggestContainer = element.querySelector('.header_search-suggest');

            this.form.addEventListener('submit', this);
            this.searchInput.addEventListener('keyup', utils.throttle(this.sendRequest, 200, this, true));
        }

        processResponse(response) {
            if (response.error) {
                this.suggestContainer.innerHTML = `<div class="header_search-section">${response.errorText}</div>`;
            } else {
                this.suggestContainer.innerHTML = response.resultHTML;
            }
        }

        sendRequest() {
            if (!this.searchInput.value || (this.query === this.searchInput.value)) {
                return;
            }

            this.query = this.searchInput.value;

            this.request.send(this.form)
                .then((response) => this.processResponse(response))
                .catch((response) => this.processResponse(response));
        }

        handleEvent(e) {
            if (e.type === 'submit') {
                e.preventDefault();
                this.sendRequest();
            }
        }
    }

    return Search;
});