/* global define */
define([
    './swiper.js',
    './AbstractModule',
    './utils.js'
], function (
    Swiper,
    AbstractModule,
    utils
) {

    /**
     * @class
     * @extends AbstractModule
     */
    class Slider extends AbstractModule {

        resizeHandler() {
            this.swiper.updateSize();
        }

        activate(element) {
            this.element = element;
            this.swiper = new Swiper(element, {
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                slidesPerView: 1,
                spaceBetween: 0,
                autoplay: 5000,
                loop: true
            });

            window.addEventListener('resize', utils.throttle(this.resizeHandler, 30, this), false);
            window.addEventListener('load', utils.throttle(this.resizeHandler, 30, this), false);
        }
    }

    return Slider;
});