/* global define, Promise */
define([
    '../AjaxRequest',
    './AbstractLayer'
], function (
    AjaxRequest,
    AbstractLayer
) {
    class ShareLinkLayer extends AbstractLayer {
        constructor() {
            super();
            this.ajaxRequest = new AjaxRequest('/linkShare');
        }

        sendRequest() {
            return this.ajaxRequest
                .send()
                .then((response) => {
                    return Promise.resolve({
                        titleHTML: "Поделиться ссылкой",
                        bodyHTML: response.resultHTML
                    })
                })
        }

        setContent(renderParams) {
            super.setContent(renderParams);

            setTimeout(() => {
                const linkShare = document.getElementById('linkShare');
                Ya.share2(linkShare, {
                    content: {
                        url: this.params.link,
                        title: this.params.title
                    }
                });
            }, 0);
        }

        getTemplate() {
            return this.sendRequest();
        }
    }

    return new ShareLinkLayer();
});