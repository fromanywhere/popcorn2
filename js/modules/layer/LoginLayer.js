/* global define, Promise */
define([
    '../AjaxRequest',
    './AbstractLayer',
    './LayerManager'
], function (
    AjaxRequest,
    AbstractLayer,
    LayerManager
) {
    class LoginLayer extends AbstractLayer {
        constructor() {
            super();
            this.ajaxRequest = new AjaxRequest('/loginForm');
        }

        sendRequest(form) {
            return this.ajaxRequest
                .send(form)
                .then((response) => {
                    if (response.success) {
                        window.location.reload();
                        return;
                    }

                    return Promise.resolve({
                        titleHTML: "Вход",
                        bodyHTML: response.resultHTML
                    })
                })
        }

        getTemplate() {
            return this.sendRequest();
        }

        activate() {
            super.activate();
            this.delegate.on('submit', 'form', this.submitHandler.bind(this));
            this.delegate.on('click', '.js-register', this.registrationRedirect.bind(this));
            this.delegate.on('click', '.js-recover', this.recoverRedirect.bind(this));
        }

        submitHandler(e, target) {
            e.preventDefault();
            this.sendRequest(target).then((data) => {
                if (data) {
                    super.setContent(data)
                }
            });
        }

        registrationRedirect(e) {
            e.preventDefault();
            LayerManager.open('RegistrationLayer');
        }

        recoverRedirect(e) {
            e.preventDefault();
            LayerManager.open('RecoverLayer');
        }
    }

    return new LoginLayer();
});