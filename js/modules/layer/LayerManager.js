/* global define, require */
define(function () {

    const scrollWidthTest = document.createElement('div');
    scrollWidthTest.className='scroll-width-test';
    const scrollWidthStyle = document.createElement('style');
    scrollWidthStyle.innerHTML = '.scroll-width-test {overflow-y: scroll;width:50px;height:50px;position:absolute;left:-9999px}';

    document.body.appendChild(scrollWidthStyle);
    document.body.appendChild(scrollWidthTest);
    const scrollWidthValue = scrollWidthTest.offsetWidth - scrollWidthTest.clientWidth;
    document.body.removeChild(scrollWidthTest);
    scrollWidthStyle.innerHTML = `body.__modal {margin-right: ${scrollWidthValue}px}`;

    class LayerManager {
        constructor() {
            this.layerRegistry = {};
            this.activeLayerInstance = null;
        }

        register(layerName, instance) {
            this.layerRegistry[layerName] = instance;
        }

        open(layerName, params = {}) {
            const layerInstance = this.layerRegistry[layerName];
            if (layerInstance) {
                layerInstance.create(params)
                    .then((instance) => {
                        if (instance) {
                            if (this.activeLayerInstance) {
                                this.activeLayerInstance.deactivate();
                            }
                            instance.activate();
                            this.activeLayerInstance = instance;

                            document.body.classList.add('__modal');
                        }
                    })
            } else {
                const module = require('./' + layerName + '.js');
                module.open(params);
            }
        }

        close(layerName = null) {
            if (!this.activeLayerInstance) {
                return;
            }

            if (layerName && (this.activeLayerInstance.constructor.name !== layerName)) {
                return;
            }

            this.activeLayerInstance.deactivate();
            this.activeLayerInstance = null;

            document.body.classList.remove('__modal');
        }
    }

    return new LayerManager();
});