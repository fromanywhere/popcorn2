/* global define, Promise */
define([
    './AbstractLayer',
    '../dom/doT',
], function (
    AbstractLayer,
    doT
) {
    const template =
        '<div class="layer_text">{{=it.text}}</div>' +
        '<div class="layer_actions">' +
            '<button class="button __wide js-deactivate">Закрыть</button>' +
        '</div>';

    class MessageLayer extends AbstractLayer {
        constructor() {
            super();
            this.bodyTemplate = doT.template(template);
        }

        getTemplate() {
            return Promise.resolve({
                titleHTML: this.params.title,
                bodyHTML: this.bodyTemplate(this.params)
            })
        }
    }

    return new MessageLayer();
});