/* global define */
define([
    './MessageLayer',
], function (
    MessageLayer
) {
    class MessageLayerHelper {

        showMessage(title, text) {
            MessageLayer.open({
                title,
                text
            });
        }
        
        showError(text) {
            MessageLayer.open({
                title: "Ошибка",
                text
            });            
        }

        showResponseError(response) {
            MessageLayer.open({
                title: "Ошибка",
                text: response.errorText
            });
        }
    }

    return new MessageLayerHelper();
});