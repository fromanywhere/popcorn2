/* global define, Promise */
define([
    '../AjaxRequest',
    './AbstractLayer',
    './LayerManager'
], function (
    AjaxRequest,
    AbstractLayer,
    LayerManager
) {
    class RegistrationLayer extends AbstractLayer {
        constructor() {
            super();
            this.ajaxRequest = new AjaxRequest('/registrationForm');
        }

        sendRequest(form) {
            return this.ajaxRequest
                .send(form)
                .then((response) => {
                    if (response.success) {
                        window.location.reload();
                        return;
                    }

                    return Promise.resolve({
                        titleHTML: "Регистрация",
                        bodyHTML: response.resultHTML
                    })
                })
        }

        getTemplate() {
            return this.sendRequest();
        }

        activate() {
            super.activate();
            this.delegate.on('submit', 'form', this.submitHandler.bind(this));
            this.delegate.on('click', '.js-login', this.loginRedirect);
        }

        submitHandler(e, target) {
            e.preventDefault();
            this.sendRequest(target).then((data) => {
                if (data) {
                    super.setContent(data)
                }
            });
        }

        loginRedirect(e) {
            e.preventDefault();
            LayerManager.open('LoginLayer');
        }
    }

    return new RegistrationLayer();
});