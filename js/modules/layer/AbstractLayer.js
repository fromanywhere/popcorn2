/* global define, require, Promise */
define([
    '../dom/doT',
    '../dom/Delegate',
    './LayerManager'
], function (
    doT,
    Delegate,
    LayerManager
) {
    const template =
        '<div class="layer_hld">' +
            '<div class="layer_overlay js-deactivate"></div>' +
            '<div class="layer_cnt">\n' +
                '<div class="layer_header">{{=it.titleHTML}}</div>' +
                '{{=it.bodyHTML}}' +
                '<div class="layer_close js-deactivate">' +
                    '<div class="icon icon_close"></div>' +
                '</div>' +
            '</div>' +
        '</div>';

    class AbstractLayer {
        constructor() {
            this.params = {};
            this.root = document.createElement('div');
            this.root.className = 'layer';
            this.template = doT.template(template);
            this.delegate = new Delegate();
            LayerManager.register(this.constructor.name, this);
        }

        create(params = {}) {
            this.params = params;
            return this.getTemplate()
                .then((renderParams) => {
                    this.setContent(renderParams);
                    this.delegate.root(this.root);
                    return Promise.resolve(this);
                })
                .catch((e) => {
                    require(['./MessageLayerHelper'], (MessageLayerHelperCircular) => {
                        MessageLayerHelperCircular.showResponseError(e);
                    })
                })
        }

        setContent(renderParams) {
            this.root.innerHTML = this.template(renderParams);
        }

        // На случай, если содержимое будет рисоваться на сервере
        getTemplate() {
            return Promise.resolve({
                titleHTML: null,
                bodyHTML: null
            })
        }

        activate() {
            this.delegate.on('click', '.js-deactivate', LayerManager.close.bind(LayerManager, null));
            document.body.appendChild(this.root);
        }

        deactivate() {
            this.delegate.destroy();
            document.body.removeChild(this.root);
        }

        open(params = {}) {
            LayerManager.open(this.constructor.name, params);
        }
    }

    return AbstractLayer;
});