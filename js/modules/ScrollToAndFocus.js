/* global define */

define([
    './ScrollTo'
], function (
    ScrollTo
) {
    class ScrollToAndFocus extends ScrollTo {

        scrollFinallyCallback(targetElement) {
            super.scrollFinallyCallback(targetElement);
            this.focusableElement.focus();
        }

        activate(element) {
            super.activate(element);

            const focusableSelector = element.getAttribute('data-focusable');
            if (focusableSelector && this.targetElement) {
                this.focusableElement = this.targetElement.querySelector(focusableSelector);
            }
        }
    }

    return ScrollToAndFocus;
});