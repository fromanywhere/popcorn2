/*global define */
/**
 * Залипающий блок, который умеет еще и скроллиться
 * @module OK/StickyBlock
 */

/**
 * Текущее применение — четвертая колонка. Кейсы к проверке:
 * 1) Корректное «залипание», если высота плашки меньше вьюпорта (залипает вверх)
 * 2) Корректное «залипание», если высота плашки больше вьюпорта (скроллится и залипает по обеим сторонам)
 * 3) Ни при каких условиях не выезжает за вернюю границу своей обертки
 * 4) Должен корректно отрабатывать переход из 3х колонок в 4 и обратно; корректной считается отрисовка во вьюпорте, а не вовне
 * 5) Должен корректно обрабатываться пересчет верхней границы обертки, позиция должна компенсироваться при необходимости
 * 6) Должен корректно обрабатываться при ненулевой начальной позиции скролла
 */

define([
    './AbstractModule',
    './ResizeObserver',
    './utils'
], function (
    AbstractModule,
    ResizeObserver,
    utils
) {
    "use strict";

    /**
     * @typedef {{}} StickyState
     * @property {string} TOP
     * @property {string} SCROLL
     * @property {string} BOTTOM
     */

    /**
     * @type {StickyState}
     */
    const states = {
        TOP: 'top',
        SCROLL: 'scroll',
        BOTTOM: 'bottom'
    };

    const scrollState = {
        UP: 'up',
        DOWN: 'down'
    };

    const GAP = 30;

    /**
     * Получить абсолютное расположение по вертикали относительно окна
     * @param {HTMLElement} node
     * @returns {number}
     */
    function getAbsoluteTopPosition(node) {
        return node.getBoundingClientRect().top + window.pageYOffset;
    }

    class StickyBlock extends AbstractModule {

        /**
         * Пересчитывает абсолютные значения отступов обертки и контейнера
         * Вызывать при изменении геометрии смежных блоков (например, размеров баннера)
         */
        init() {
            this.scrollDelta = 0;
            this.scrollPosition = window.pageYOffset;
            this.scrollDirection = null;

            this._setTopShift(0);
            this._setState(states.SCROLL);
        }

        render() {
            const scrollPosition = window.pageYOffset;
            const viewportHeight = window.innerHeight;
            const viewportHeightWithoutToolbar = viewportHeight - this.fixedTopThreshold;

            const mainColumnObject = this.mainContentColumn.getBoundingClientRect();
            const rectObject = this.stickyContainer.getBoundingClientRect();
            const mainColumnHeight = mainColumnObject.height;
            const stickyHeight = rectObject.height;
            const wrapperAbsoluteTop = getAbsoluteTopPosition(this.stickyWrapper);

            // Панель не меньше высоты документа
            if (document.documentElement.scrollHeight <= (wrapperAbsoluteTop + stickyHeight)) {
                this.init();
                return;
            }
            
            // если залипающий блок выше основного контента, то и залипать не надо
            if (stickyHeight > mainColumnHeight) {
                this.init();
                return;
            }

            const isFooterReached = mainColumnObject.bottom <= rectObject.bottom;

            if (stickyHeight <= viewportHeightWithoutToolbar) {
                // Стики-панель меньше вьюпорта
                if (scrollPosition > (wrapperAbsoluteTop - this.fixedTopThreshold)) {

                    if (this.scrollDirection === scrollState.UP) {

                        if (this.state === states.SCROLL) {
                            if (rectObject.top < this.fixedTopThreshold) {
                                this._setTopShift(mainColumnHeight - stickyHeight - GAP);
                                this._setState(states.SCROLL);
                            } else {
                                this._setState(states.TOP);
                            }
                        }
                    }

                    if (this.scrollDirection === scrollState.DOWN) {
                        switch (this.state) {
                            case (states.TOP):
                                if (isFooterReached) {
                                    this._setTopShift(mainColumnHeight - stickyHeight - GAP);
                                    this._setState(states.SCROLL);
                                } else {
                                    this._setState(states.TOP);
                                }
                                break;
                            case (states.SCROLL):
                                if (!isFooterReached) {
                                    this._setState(states.TOP);
                                }
                                break;

                        }
                    }

                } else {
                    this.init();
                }
            } else {
                // Стики-панель больше вьюпорта

                if (this.scrollDirection === scrollState.UP) {
                    // не выше, чем обертка
                    if ((wrapperAbsoluteTop - this.fixedTopThreshold) > scrollPosition) {
                        this.init();
                        return;
                    }

                    switch (this.state) {
                        case (states.SCROLL):
                            if (rectObject.top > this.fixedTopThreshold) {
                                this._setState(states.TOP);
                            }
                            break;
                        case (states.BOTTOM):
                            // this.scrollDelta - учитываем, что скролл уже произошел
                            this._setTopShift(scrollPosition + rectObject.top - wrapperAbsoluteTop - this.scrollDelta);
                            this._setState(states.SCROLL);
                            break;
                    }
                }

                if (this.scrollDirection === scrollState.DOWN) {

                    switch (this.state) {
                        case (states.TOP):
                            this._setTopShift(scrollPosition + rectObject.top - wrapperAbsoluteTop);
                            this._setState(states.SCROLL);
                            break;
                        case (states.SCROLL):
                            // не ниже нижней границы вьюпорта
                            if (!isFooterReached && (rectObject.bottom <= viewportHeight)) {
                                this._setState(states.BOTTOM);
                            }
                            break;
                        case (states.BOTTOM):
                            if (isFooterReached) {
                                this._setTopShift(mainColumnHeight - stickyHeight - GAP);
                                this._setState(states.SCROLL);
                            }
                            break;
                    }
                }

                // Загрузка произошла, позиция скролла фактически ненулевая, а события на скролл не случилось
                // (Хром пытается сохранить предыдущую позицию скролла, если загрузка достаточно быстра)
                if (this.scrollDirection === null && this.topShift === 0) {
                    if (isFooterReached) {
                        this._setTopShift(mainColumnHeight - stickyHeight - GAP);
                        this._setState(states.SCROLL);
                        return;
                    }

                    if (scrollPosition > (wrapperAbsoluteTop - this.fixedTopThreshold)) {
                        this._setState(states.TOP);
                    }
                }
            }

        }

        _onScroll() {
            const position = window.pageYOffset;
            if (position === this.scrollPosition) {
                return;
            }

            this.scrollDelta = position - this.scrollPosition;
            this.scrollDirection  = this.scrollDelta > 0
                ? scrollState.DOWN
                : scrollState.UP;

            this.scrollPosition = position;

            this.render();
        }

        /**
         *
         * @param {string} state
         * @private
         */
        _setState(state) {
            const stickyContainerClassList = this.stickyContainer.classList;
            switch (state) {
                case states.TOP:
                    stickyContainerClassList.remove('__fixed-bottom');
                    stickyContainerClassList.add('__fixed-top');
                    this.stickyContainer.style.marginTop = '0px';
                    break;
                case states.SCROLL:
                    stickyContainerClassList.remove('__fixed-top', '__fixed-bottom');
                    this.stickyContainer.style.marginTop = this.topShift + 'px';
                    break;
                case states.BOTTOM:
                    stickyContainerClassList.remove('__fixed-top');
                    stickyContainerClassList.add('__fixed-bottom');
                    this.stickyContainer.style.marginTop = '0px';
                    break;
            }

            this.state = state;
        }

        /**
         *
         * @param {number} value
         * @private
         */
        _setTopShift(value) {
            if (this.topShift !== value) {
                this.topShift = value < 0
                    ? 0
                    : value;
            }
        }

        activate(element) {
            if (utils.isTouch()) {
                return;
            }

            this.fixedTopThreshold = 50; // tmp

            // Сдвиг контейнера относительно обертки
            this.topShift = 0;

            /** @type {HTMLElement} */
            this.stickyWrapper = element;
            /** @type {HTMLElement} */
            this.stickyContainer = element.firstElementChild;
            /** @type {HTMLElement} */
            this.mainContentColumn = document.querySelector('.wrapper_main');

            /** @type {ResizeObserver} */
            const mainColumnObserver = new ResizeObserver(() => {
                this.render();
            });
            mainColumnObserver.observe(this.mainContentColumn);

            /** @type {ResizeObserver} */
            const columnObserver = new ResizeObserver(() => {
                this.render();
            });
            columnObserver.observe(this.stickyContainer);

            this.scrollHandler = utils.throttle(this._onScroll, 10, this);
            this.resizeHandler = utils.throttle(this.render, 10, this);

            this.init();
            this.render();

            window.addEventListener('scroll', this.scrollHandler, false);
            window.addEventListener('resize', this.resizeHandler, false);
        }
    }

    return StickyBlock;
});