/* global require */
const hooks = document.getElementsByClassName('js-hook');
Array.prototype.slice.call(hooks).forEach((hookNode) => {
    const hookName = hookNode.getAttribute('data-module');
    if (hookName) {
        const hookNames = hookName.split(' ');
        hookNames.forEach((name) => {
            const Module = require('./modules/' + name + '.js');
            const instance = new Module();
            instance.activate(hookNode);
        });
    }
});

function executor(arg1, arg2) {
    let modules = [];
    if (Array.isArray(arg1) && arg1.length) {
        modules = arg1.map((dep) => {
            return require('./modules/' + dep + '.js');
        });
    }

    let fun = arg2 || arg1;

    if (typeof fun === "function") {
        fun.apply(fun, modules);
    }
}

window.onloadCallback.run = executor;
window.onloadCallback.list.forEach((task) => {
    executor(task.arg1, task.arg2);
});