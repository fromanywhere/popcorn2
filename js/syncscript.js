const isManualScrollRestoration = ('scrollRestoration' in history);
const hasSessionHistory = !!window.sessionStorage.getItem('scroll');
const height = document.getElementById('header-navigation').clientHeight;

if (hasSessionHistory && height > 70) {
    requestAnimationFrame(() => {
        if (isManualScrollRestoration) {
            history.scrollRestoration = 'manual';
        }
        window.scroll(0, height);
    });
}
window.sessionStorage.setItem('scroll', 'true');

window.onloadCallback = {
    list: [],
    run: function (arg1, arg2) {
        this.list.push({
            arg1,
            arg2
        })
    }
};