"use strict";

var isManualScrollRestoration = 'scrollRestoration' in history;
var hasSessionHistory = !!window.sessionStorage.getItem('scroll');
var height = document.getElementById('header-navigation').clientHeight;

if (hasSessionHistory && height > 70) {
  requestAnimationFrame(function () {
    if (isManualScrollRestoration) {
      history.scrollRestoration = 'manual';
    }

    window.scroll(0, height);
  });
}

window.sessionStorage.setItem('scroll', 'true');
window.onloadCallback = {
  list: [],
  run: function run(arg1, arg2) {
    this.list.push({
      arg1: arg1,
      arg2: arg2
    });
  }
};